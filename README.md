# Deployments

Automated Kubernetes and Helm deployments using Terraform

## Prerequisites

- [Terraform](https://www.terraform.io/)
- A Kubernetes cluster

## Usage

```
$ cd terraform/
$ terraform init
$ terraform apply -var-file config.tfvars
```

Currently Terraform expects the `kubeconfig` file to be located at the relative path `../provisioning/secrets/kubeconfig`. This can be changed in `terraform/config.tfvars`.

## Documentation

https://fasoz.gitlab.io/documentation/operations-onboarding/infrastructure/
