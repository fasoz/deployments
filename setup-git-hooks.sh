#!/bin/bash
echo "#/bin/bash
cd terraform/
terraform fmt -check -recursive
terraform validate" > .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
