resource "kubernetes_namespace" "fallarchiv" {
  metadata {
    name = "fallarchiv"
  }
}

resource "kubernetes_deployment" "fallarchiv_backend" {
  metadata {
    name      = "backend"
    namespace = kubernetes_namespace.fallarchiv.metadata[0].name
    labels = {
      app = "backend"
    }
  }

  spec {
    replicas = 4

    selector {
      match_labels = {
        app = "backend"
      }
    }

    strategy {
      type = "RollingUpdate"
    }


    template {
      metadata {
        annotations = {
          "elasticsearch-master-http-client" = "true" # needed for the Elasticsearch network policy
        }
        labels = {
          app = "backend"
        }
      }
      spec {
        container {
          image             = "registry.gitlab.com/fasoz/backend:v0.5.7"
          image_pull_policy = "Always"
          name              = "backend"

          port {
            name           = "http"
            container_port = 3001
          }

          volume_mount {
            name       = "fallarchiv-prod-config"
            mount_path = "/usr/src/app/config"
          }
        }

        image_pull_secrets {
          name = "regcred"
        }

        volume {
          name = "fallarchiv-prod-config"
          secret {
            secret_name  = "fallarchiv-prod-config"
            default_mode = "0400"
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fallarchiv_frontend" {
  metadata {
    name      = "frontend"
    namespace = kubernetes_namespace.fallarchiv.metadata[0].name
    labels = {
      app = "frontend"
    }
  }

  spec {
    replicas = 4

    selector {
      match_labels = {
        app = "frontend"
      }
    }

    template {
      metadata {
        labels = {
          app = "frontend"
        }
      }
      spec {
        container {
          image             = "registry.gitlab.com/fasoz/frontend:v0.5.7"
          image_pull_policy = "Always"
          name              = "frontend"

          port {
            name           = "http"
            container_port = 80
          }
        }

        image_pull_secrets {
          name = "regcred"
        }
      }
    }
  }
}

resource "kubernetes_service" "fallarchiv_backend" {
  metadata {
    name      = "backend"
    namespace = kubernetes_namespace.fallarchiv.metadata[0].name
  }

  spec {
    port {
      name        = "backend-http"
      port        = 3001
      target_port = "http"
    }

    selector = {
      app = "backend"
    }
  }
}

resource "kubernetes_service" "fallarchiv_frontend" {
  metadata {
    name      = "frontend"
    namespace = kubernetes_namespace.fallarchiv.metadata[0].name
  }

  spec {
    port {
      name        = "frontend-http"
      port        = 80
      target_port = "http"
    }

    selector = {
      app = "frontend"
    }
  }
}

resource "kubernetes_ingress" "fallarchiv" {
  metadata {
    name      = "fallarchiv"
    namespace = kubernetes_namespace.fallarchiv.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer"                     = "letsencrypt-prod"
      "ingress.kubernetes.io/ssl-redirect"                 = true
      "ingress.kubernetes.io/proxy-body-size"              = "512m"
      "kubernetes.io/ingress.class"                        = "traefik"
      "traefik.ingress.kubernetes.io/redirect-regex"       = "^https://fallarchiv.de/(.*)"
      "traefik.ingress.kubernetes.io/redirect-replacement" = "https://www.fallarchiv.de/$1"
      "traefik.ingress.kubernetes.io/rate-limit"           = <<EOF
        extractorfunc: client.ip
        rateset:
          rateset1:
            period: 5s
            average: 10
            burst: 20
          rateset1:
            period: 60s
            average: 40
            burst: 80
        EOF
    }
  }

  spec {
    rule {
      host = "fallarchiv.de"

      http {
        path {
          path = "/api"

          backend {
            service_name = "backend"
            service_port = "backend-http"
          }
        }
        path {
          path = "/"

          backend {
            service_name = "frontend"
            service_port = "frontend-http"
          }
        }
      }
    }

    rule {
      host = "www.fallarchiv.de"

      http {
        path {
          path = "/api"

          backend {
            service_name = "backend"
            service_port = "backend-http"
          }
        }
        path {
          path = "/"

          backend {
            service_name = "frontend"
            service_port = "frontend-http"
          }
        }
      }
    }

    tls {
      hosts       = ["fallarchiv.de", "www.fallarchiv.de"]
      secret_name = "fallarchiv-cert"
    }
  }
}
