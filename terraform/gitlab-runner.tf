resource "helm_release" "gitlab_runner" {
  name             = "gitlab-runner"
  namespace        = "gitlab"
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  create_namespace = true

  values = [
    file("../helm/values-gitlab-runner.yaml")
  ]
}
