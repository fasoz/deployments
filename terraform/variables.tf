variable "admin_email" {

}

variable "domain" {
  default = "127.0.0.1.nip.io"
}

variable "kube_config_path" {
  default = "~./kube/config"
}
