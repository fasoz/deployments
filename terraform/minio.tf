resource "random_password" "access_key" {
  length  = 20
  special = false
}

resource "random_password" "secret_key" {
  length  = 40
  special = false
}

resource "kubernetes_namespace" "minio" {
  metadata {
    name = "minio"
  }
}

resource "helm_release" "minio" {
  name       = "minio"
  namespace  = kubernetes_namespace.minio.metadata[0].name
  repository = "https://helm.min.io/"
  chart      = "minio"
  version    = "8.0.10"

  values = [
    file("../helm/values-minio.yaml")
  ]

  set {
    name  = "accessKey"
    value = sensitive(random_password.access_key.result)
  }

  set {
    name  = "secretKey"
    value = sensitive(random_password.secret_key.result)
  }
}

data "http" "grafana_minio_dashboard" {
  url = "https://grafana.com/api/dashboards/12563/revisions/4/download"
}

resource "kubernetes_config_map" "grafana_minio_dashboard" {
  metadata {
    name      = "grafana-minio-dashboard"
    namespace = "monitoring"
    labels = {
      "grafana_dashboard" = "grafana_minio_dashboard"
    }
  }

  data = {
    "minio.json" = data.http.grafana_minio_dashboard.body
  }
}


output "minio_access_key" {
  value     = random_password.access_key.result
  sensitive = true
}

output "minio_secret_key" {
  value     = random_password.secret_key.result
  sensitive = true
}
