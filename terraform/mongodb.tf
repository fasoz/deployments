resource "random_password" "mongodb_root" {
  length  = 24
  special = false
}

resource "kubernetes_namespace" "mongodb" {
  metadata {
    name = "mongodb"
  }
}

resource "helm_release" "mongodb" {
  name       = "mongodb"
  namespace  = kubernetes_namespace.mongodb.metadata[0].name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mongodb"
  version    = "10.26.0"

  values = [
    file("../helm/values-mongodb.yaml")
  ]

  set {
    name  = "auth.rootPassword"
    value = sensitive(random_password.mongodb_root.result)
  }
}

output "mongodb_root_password" {
  value     = random_password.mongodb_root.result
  sensitive = true
}
