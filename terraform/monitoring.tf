resource "random_password" "grafana" {
  length  = 24
  special = false
}

resource "helm_release" "prometheus_stack" {
  name             = "kube-prometheus-stack"
  namespace        = "monitoring"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  version          = "18.0.7"
  create_namespace = true

  values = [
    file("../helm/values-kube-prometheus-stack.yaml")
  ]

  set {
    name  = "grafana.ingress.hosts"
    value = "{grafana.${var.domain}}"
  }

  set {
    name  = "grafana.ingress.tls[0].hosts"
    value = "{grafana.${var.domain}}"
  }

  set {
    name  = "grafana.adminPassword"
    value = sensitive(random_password.grafana.result)
  }
}

output "grafana_admin_password" {
  value     = random_password.grafana.result
  sensitive = true
}
