resource "kubernetes_namespace" "logging" {
  metadata {
    name = "logging"
  }
}

resource "helm_release" "loki-stack" {
  name       = "loki-stack"
  namespace  = kubernetes_namespace.logging.metadata[0].name
  repository = "https://grafana.github.io/loki/charts"
  chart      = "loki-stack"
}
