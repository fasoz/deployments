#!/bin/bash
# Forwards a local port to the MongoDB server running in the Kubernetes cluster.
# Locally starts a mongo-express container connected to the MongoDB server.
# Open http://localhost:8081 in your browser to access it.

PASSWORD=`terraform output -state ../terraform/terraform.tfstate -raw mongodb_root_password`

kubectl -n mongodb port-forward service/mongodb 27017:27017 &
PF_PID=$!
docker run -it --rm --net=host -e ME_CONFIG_MONGODB_SERVER=localhost -e ME_CONFIG_MONGODB_ADMINUSERNAME=root -e ME_CONFIG_MONGODB_ADMINPASSWORD=${PASSWORD} -p 8081:8081 mongo-express
kill ${PF_PID}
