#!/bin/bash
# Backs the MongoDB prod database and the Minio prod bucket to a local folder.
# This is a lousy way to do backups. But there is no way to store backups in our data center and storing backups off-site has the usual privacy problems.
# Related GitLab issue: https://gitlab.com/fasoz/deployments/-/issues/1

TIMESTAMP=`date --iso-8601=seconds`
BACKUP_FOLDER="${PWD}/backup-fallarchiv/${TIMESTAMP}"

MONGODB_PASSWORD=`terraform output -state ../terraform/terraform.tfstate -raw mongodb_root_password`
export AWS_ACCESS_KEY_ID=`terraform output -state ../terraform/terraform.tfstate -raw minio_access_key`
export AWS_SECRET_ACCESS_KEY=`terraform output -state ../terraform/terraform.tfstate -raw minio_secret_key`

# Kubernetes port forwardings
kubectl -n minio port-forward service/minio 19000:9000 &
MINIO_PF_PID=$!
kubectl -n mongodb port-forward service/mongodb 47017:27017 &
MONGODB_PF_PID=$!

# backup Minio prod bucket
mkdir -p ${BACKUP_FOLDER}/minio
aws s3 --endpoint-url http://localhost:19000 cp s3://frafasoz-casefiles-prod/ ${BACKUP_FOLDER}/minio --recursive
cd ${BACKUP_FOLDER}/minio
tar -c --lzma -f ../minio.tar.lzma ./*
cd ..
rm -rf minio

# backup MongoDB prod database
mongodump --host localhost --port 47017 --authenticationDatabase admin -u root -p ${MONGODB_PASSWORD} --db frafasoz-prod --out ${BACKUP_FOLDER}/mongodb
cd ${BACKUP_FOLDER}/mongodb
tar -c --lzma -f ../mongodb.tar.lzma ./*
cd ..
rm -rf mongodb

# stop port Kubernetes forwardings
kill ${MINIO_PF_PID} ${MONGODB_PF_PID}
