#/bin/bash
# Gets the login credentials for Minio from Terraform, then forwards a port to the Minio instance running in Kubernetes.

echo "Open http://localhost:9000 in your browser and enter the following credentials"

cd ../terraform
echo "Access key: `terraform output -raw minio_access_key`"
echo "Secret key:  `terraform output -raw minio_secret_key`"

echo "Initiating port forwarding to Kuberentes... (press CTRL-C to exit)"
kubectl -n minio port-forward service/minio 9000:9000
